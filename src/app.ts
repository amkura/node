// in ts we can use the same import syntax as in the browser
import express, { Request, Response, NextFunction} from 'express';
import todoRoutes from './routes/todos';
import { json } from 'body-parser';
const app = express();

app.use('/todos', todoRoutes);
app.use(json()); // parses the body of incoming req and exracts JSON data and populates req.body

// will fire whenever errored
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  res.status(500).json({message: err.message});
});

app.listen(3000);

